%%%-------------------------------------------------------------------
%%% @author root
%%% @copyright (C) 2016, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 16. jun 2016 20:29
%%%-------------------------------------------------------------------
-module(peer).
-author("root").

%% API
-export([start/2, behaviour/2, stop/1, timeout/1, auto_commit/1]).

-record(attr, {
  file_name,
  register_name,
  master,
  local = [],
  tag = commit_tag:new(),
  peers,
  monitors,
  gui
}).

-define(AUTO_COMMIT_INTERVAL, 300).

start(ClientName, Data) ->
  Main = self(),
  spawn(fun() -> init(ClientName, Data, Main) end).

init(ClientName, {file_name, FileName}, Main) ->
  This = #attr{
    file_name = FileName,
    register_name = utils:generate_register_name(FileName),
    peers = [],
    monitors = [],
    master = []
  },
  launch_client(This, ClientName, Main);

init(ClientName, {share_key, ShareKey}, Main) ->
  case fork(ShareKey) of
    {success, Peers, Monitors, Commits, FileName} ->
      This = #attr{
        file_name = FileName,
        register_name = utils:generate_register_name(FileName),
        peers = Peers,
        monitors = Monitors,
        master = Commits
      },
      launch_client(This, ClientName, Main);
    fail -> Main ! {fork_fail, ShareKey}
  end.

stop(This) ->
  unregister(This#attr.register_name),
  lists:foreach(fun({_Peer, Monitor}) -> demonitor(Monitor) end, This#attr.monitors),
  This#attr.gui ! stop,
  logger:d("Closing ~s", [This#attr.file_name]).

timeout(_This) -> ?AUTO_COMMIT_INTERVAL.

behaviour(This, Message) ->
  case Message of
    {new_peer, Peer} -> add_peer(This, Peer);
    {gui, Op} ->
      update_local(This, Op);
    {commit, Peer, Commit} ->
      CommitTag = commit:get_tag(Commit),
      case commit_tag:safe(CommitTag, This#attr.tag) of
        false ->
          {state, This2} = commit(This, [Peer]),
          {Master, Diff} = commit:merge(This2#attr.master, Commit),
%%          case Diff of
%%            [] -> ok;
%%            _Diff -> Peer ! {commit, self(), Diff}
%%          end,
          Ops = lists:reverse(commit:get_ops(Master)),
          logger:d("Ops ~w", [Ops]),
          This2#attr.gui ! {ops, Ops},
          {state, This2#attr{master = Master, tag = commit_tag:merge(CommitTag, This2#attr.tag)}};
        true -> ignored
      end;
    {get_document, Requester} ->
      Requester ! {document, This#attr.peers, This#attr.master, This#attr.file_name};
    {'DOWN', _, _, Peer, _} -> remove_peer(This, Peer);
    _ -> logger:e("Unhandled message: ~w", [Message])
  end.

auto_commit(This) -> commit(This, []).

commit(This, Excluded) ->
  case This#attr.local of
    [] -> {state, This};
    _Local ->
      utils:broadcast(lists:subtract(This#attr.peers, Excluded), {commit, self(), This#attr.local}),
      Master = This#attr.local ++ This#attr.master,
      {state, This#attr{master = Master, local = []}}
  end.

add_peer(This, Peer) ->
  logger:d("Added peer ~w", [Peer]),
  {state, This#attr{peers = [Peer | This#attr.peers], monitors = [track(Peer) | This#attr.monitors]}}.

remove_peer(This, Peer) ->
  logger:d("Removed peer ~w", [Peer]),
  Peers = lists:delete(Peer, This#attr.peers),
  Monitors = lists:keydelete(Peer, 1, This#attr.monitors),
  {state, This#attr{peers = Peers, monitors = Monitors}}.

update_local(This, Op) ->
  Tag = commit_tag:inc(self(), This#attr.tag),
  Local = [{Tag, Op} | This#attr.local],
  {state, This#attr{local = Local, tag = Tag}}.

fork(ShareKey) ->
  try utils:decode_share_key(ShareKey) of
    FirstPeer ->
      logger:d("Decode successfull: ~w", [FirstPeer]),
      connect_to(FirstPeer)
  catch
    error:_ -> fail
  end.

connect_to(FirstPeer) ->
  FirstPeer ! {get_document, self()},
  Monitor = monitor(process, FirstPeer),
  receive
    {'DOWN', Monitor, _, FirstPeer, _} -> fail;
    {document, Peers, Ops, Filename} ->
      demonitor(Monitor),
      AllPeers = [FirstPeer | Peers],
      {success, AllPeers, make_monitors(AllPeers), Ops, Filename}
  end.

track(Peer) -> {Peer, monitor(process, Peer)}.

make_monitors([]) -> [];
make_monitors([Peer | Peers]) -> [track(Peer) | make_monitors(Peers)].

launch_client(This, ClientName, Main) ->
  case whereis(This#attr.register_name) of
    undefined ->
      logger:d("Registering as ~w", [This#attr.register_name]),
      register(This#attr.register_name, self()),
      Main ! {new_client, This#attr.register_name, self()},
      ShareKey = utils:encode_share_key(This#attr.register_name),
      logger:d("Document ops: ~w", [(This#attr.master)]),
      Gui = peer_gui:start(self(), ClientName, This#attr.file_name, ShareKey),
      Gui ! {ops, lists:reverse(commit:get_ops(This#attr.master))},
      lists:foreach(fun(Peer) -> Peer ! {new_peer, self()} end, This#attr.peers),
      lifecycle:init_loop_with_after(This#attr{gui = Gui}, module_info(module), [{after_expression, timeout}, {after_receive, auto_commit}]);
    _ -> logger:d("File ~s already open", [This#attr.file_name]), Main ! {client_fail, This#attr.file_name}
  end.

