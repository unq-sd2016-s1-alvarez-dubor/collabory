%%%-------------------------------------------------------------------
%%% @author Emanuel
%%% @copyright (C) 2016, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 14. jul 2016 13:32
%%%-------------------------------------------------------------------
-module(commit_tag).
-author("Emanuel").

%% API
-export([new/0, inc/2, merge/2, get_peers/1, safe/2]).

new() -> [].

inc(Peer, Tag) ->
  case lists:keyfind(Peer, 1, Tag) of
    {Peer, X} -> New = {Peer, X + 1};
    false -> New = {Peer, 1}
  end,
  lists:keystore(Peer, 1, Tag, New).

merge([], Tag) -> Tag;
merge([{Peer, X} | TagA], TagB) ->
  case lists:keytake(Peer, 1, TagB) of
    {value, {Peer, Y}, TagB2} ->
      Head = {Peer, max(X, Y)},
      Tail = merge(TagA, TagB2);
    false ->
      Head = {Peer, X},
      Tail = merge(TagA, TagB)
  end,
  [Head | Tail].

get_peers(Tag) -> lists:map(fun({Peer, _X}) -> Peer end, Tag).

safe(TagA, TagB) -> lists:all(fun({Peer, X}) ->
  case lists:keyfind(Peer, 1, TagB) of
    {Peer, Y} -> X < Y;
    false -> false
  end end, TagA).

