%%%-------------------------------------------------------------------
%%% @author root
%%% @copyright (C) 2016, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 09. jun 2016 19:15
%%%-------------------------------------------------------------------
-module(peer_gui).
-author("root").

%% API
-export([start/4, refresh_frame/1, behaviour/2]).

-include_lib("wx/include/wx.hrl").

-record(attr, {
  client,
  frame,
  text,
  textController
}).

start(ClientPID, ClientName, TargetFile, ShareKey) ->
  spawn(fun() -> init(ClientPID, ClientName, TargetFile, ShareKey) end).

init(ClientPID, ClientName, TargetFile, ShareKey) ->
  Window = make_window(ClientName, TargetFile, ShareKey),
  This = Window#attr{client = ClientPID},
  wxFrame:show(This#attr.frame),
  lifecycle:init_loop(This, module_info(module), [
    {before_receive, refresh_frame}
  ]).

refresh_frame(This) ->
  wxFrame:refresh(This#attr.frame).

behaviour(This, Message) ->
  Key = utils:message_key(Message),
  case Key of
    close_window -> This#attr.client ! stop;
    key_up ->
      NewText = wxTextCtrl:getValue(This#attr.textController),
      case ops:diff(This#attr.text, NewText) of
        [] -> no_operation;
        DiffOps ->
          send_ops(This#attr.client, DiffOps),
          {state, This#attr{text = NewText}}
      end;
    {ops, Ops} ->
      {state, This#attr{text = update_text(This, Ops)}};
    _ -> logger:e("Unhandled message: ~w", [Message])
  end.

send_ops(Client, Ops) -> Client ! {gui, Ops}.

update_text(This, Ops) ->
  OldInsertionPoint = wxTextCtrl:getInsertionPoint(This#attr.textController),
%%  {NewText, InsertionPoint} = ops:apply(Ops, wxTextCtrl:getValue(This#attr.textController), OldInsertionPoint),
  {NewText, InsertionPoint} = ops:apply_all(Ops, "", OldInsertionPoint),
  wxTextCtrl:setValue(This#attr.textController, NewText),
  wxTextCtrl:setInsertionPoint(This#attr.textController, InsertionPoint),
  NewText.

make_window(ClientName, TargetFile, ShareKey) ->
  Width = 480,
  Height = 480,
  BorderPixels = min(20, min(Width, Height) * 0.05),
  WindowTitle = io_lib:format("Collabory - Working as ~s", [ClientName]),
  Server = wx:new(),
  Frame = wxFrame:new(Server, -1, WindowTitle, [{size, {Width, Height}}]),
  MainPanel = wxPanel:new(Frame),
  TextBox = wxTextCtrl:new(MainPanel, 1001, [{style, ?wxTE_MULTILINE}]),
  wxTextCtrl:connect(TextBox, key_up),
  wxTextCtrl:connect(TextBox, command_text_cut, [{skip, true}]),
  wxTextCtrl:connect(TextBox, command_text_paste, [{skip, true}]),
  wxEvtHandler:connect(Frame, close_window, [{skip, true}]),
  CurrentFileLabel = wxStaticText:new(MainPanel, 101, "Current file: "),
  TargetFileLabel = wxStaticText:new(MainPanel, 102, TargetFile),
  ShareKeyLabel = wxStaticText:new(MainPanel, 103, "Share key: "),
  ShareKeyTextBox = wxTextCtrl:new(MainPanel, 1002, [{style, ?wxTE_READONLY}]),
  wxTextCtrl:appendText(ShareKeyTextBox, ShareKey),
  MainSizer = wxBoxSizer:new(?wxVERTICAL),
  FileNameSizer = wxBoxSizer:new(?wxHORIZONTAL),
  ShareKeySizer = wxBoxSizer:new(?wxHORIZONTAL),
  wxSizer:add(FileNameSizer, CurrentFileLabel, [{border, BorderPixels}]),
  wxSizer:add(FileNameSizer, TargetFileLabel, [{border, BorderPixels}]),
  wxSizer:add(ShareKeySizer, ShareKeyLabel, [{border, BorderPixels}]),
  wxSizer:add(ShareKeySizer, ShareKeyTextBox, [{border, BorderPixels}]),
  wxSizer:add(MainSizer, FileNameSizer, [{border, BorderPixels}, {flag, ?wxALL bor ?wxCENTER}]),
  wxSizer:add(MainSizer, ShareKeySizer, [{border, BorderPixels}, {flag, ?wxALL bor ?wxCENTER}]),
  wxSizer:add(MainSizer, TextBox, [{proportion, 1}, {border, BorderPixels}, {flag, ?wxALL bor ?wxEXPAND}]),
  wxPanel:setSizer(MainPanel, MainSizer),
  #attr{frame = Frame, text = "", textController = TextBox}.
