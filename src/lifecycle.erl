%%%-------------------------------------------------------------------
%%% @author root
%%% @copyright (C) 2016, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 23. jun 2016 18:07
%%%-------------------------------------------------------------------
-module(lifecycle).
-author("root").

%% API
-export([init_loop/3, init_loop/2, init_loop_with_after/2, init_loop_with_after/3]).

-record(attr, {
  state,
  module,
  before_receive,
  stop,
  behaviour,
  after_expression,
  after_receive
}).


init_loop(State, Module) -> init_loop(State, Module, []).
init_loop(State, Module, Definitions) ->
  This = #attr{
    state = State,
    module = Module,
    before_receive = proplists:get_value(before_receive, Definitions, optional(Module, before_receive)),
    stop = proplists:get_value(stop, Definitions, optional(Module, stop)),
    behaviour = proplists:get_value(behaviour, Definitions, required(Module, behaviour))
  },
  loop(This).

init_loop_with_after(State, Module) -> init_loop_with_after(State, Module, []).
init_loop_with_after(State, Module, Definitions) ->
  This = #attr{
    state = State,
    module = Module,
    before_receive = proplists:get_value(before_receive, Definitions, optional(Module, before_receive)),
    stop = proplists:get_value(stop, Definitions, optional(Module, stop)),
    behaviour = proplists:get_value(behaviour, Definitions, required(Module, behaviour)),
    after_expression = proplists:get_value(after_expression, Definitions, required(Module, after_expression)),
    after_receive = proplists:get_value(after_receive, Definitions, required(Module, after_receive))
  },
  loop_with_after(This).

loop(This) ->
  This2 = before_receive(This),
  receive
    stop -> stop(This2);
    Message ->
      loop(behaviour(This2, Message))
  end.

loop_with_after(This) ->
  This2 = before_receive(This),
  receive
    stop -> stop(This2);
    Message ->
      loop_with_after(behaviour(This2, Message))
  after after_expression(This2) ->
    loop_with_after(after_receive(This2))
  end.

invoke(This, Method) -> invoke(This, Method, []).
invoke(This, not_implemented, _Arguments) -> This;
invoke(_This, {lifecycle_error, Key}, _Arguments) -> erlang:error("Cant find required function ~w", [Key]);
invoke(This, Method, Arguments) -> update_state(This, apply(This#attr.module, Method, [This#attr.state | Arguments])).

execute(_This, {lifecycle_error, Key}) -> erlang:error("Cant find required function ~w", [Key]);
execute(This, Expression) -> apply(This#attr.module, Expression, [This#attr.state]).

before_receive(This) -> invoke(This, This#attr.before_receive).
stop(This) -> invoke(This, This#attr.stop).
behaviour(This, Message) -> invoke(This, This#attr.behaviour, [Message]).
after_expression(This) -> execute(This, This#attr.after_expression).
after_receive(This) -> invoke(This, This#attr.after_receive).

update_state(This, {state, NewState}) -> This#attr{state = NewState};
update_state(This, _) -> This.

required(Module, Key) ->
  case lists:keymember(Key, 1, Module:module_info(functions)) of
    true -> Key;
    false -> {lifecycle_error, Key}
  end.

optional(Module, Key) ->
  case lists:keymember(Key, 1, Module:module_info(functions)) of
    true -> Key;
    false -> not_implemented
  end.
