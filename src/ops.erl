%%%-------------------------------------------------------------------
%%% @author root
%%% @copyright (C) 2016, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 30. jun 2016 18:35
%%%-------------------------------------------------------------------
-module(ops).
-author("root").

%% API
-export([diff/2, apply/3, merge/2, apply_all/3]).

diff(T1, T2) ->
  {DigestedT1, DigestedT2, Right} = findDiffPosition(T1, T2, 0),
  {_, DiffText, Left} = findDiffPosition(
    lists:reverse(DigestedT1),
    lists:reverse(DigestedT2), 0
  ),
  NewText = lists:reverse(DiffText),
  OldTextLen = length(T1) - Right - Left,
  OldText = if
              (OldTextLen > 0) and (Right >= 0) -> lists:sublist(T1, Right + 1, OldTextLen);
              (length(T2) == 0) and (length(T1) > 0) -> T1;
              true -> "" end,
  logger:d("Old: ~s New: ~s Left: ~w Right: ~w OldTextLen: ~w T1: ~s T2: ~s", [OldText, NewText, Left, Right, OldTextLen, T1, T2]),
  Ops = case {NewText, OldText, Left, Right} of
          {[], [], _, _} -> [];
          {New, [], From, To} -> [{retain, To}, {insert, New}, {retain, From}];
          {[], Old, From, To} -> [{retain, To}, {delete, Old}, {retain, From}];
          {New, Old, From, To} -> [{retain, To}, {delete, Old}, {insert, New}, {retain, From}]
        end,
  lists:filter(fun(Op) -> case Op of {retain, 0} -> false; _ -> true end end, Ops).


findDiffPosition([C | T1], [C | T2], Index) -> findDiffPosition(T1, T2, Index + 1);
findDiffPosition(T1, T2, Index) -> {T1, T2, Index}.

apply_all([], Text, InsertionPoint) -> {Text, InsertionPoint};
apply_all([Op | Ops], Text, InsertionPoint) ->
  {Text2, InsertionPoint2} = ops:apply(Op, Text, InsertionPoint),
  apply_all(Ops, Text2, InsertionPoint2).

apply(Ops, Text, InsertionPoint) -> apply(Ops, Text, 0, InsertionPoint).
apply([], Text, _, InsertionPoint) -> {Text, InsertionPoint};
apply([Op | Ops], Text, Cursor, InsertionPoint) ->
  case Op of
    {retain, 0} -> apply(Ops, Text, Cursor, InsertionPoint);
    {retain, Quantity} when length(Text) >= Quantity ->
      {ResultText, ResultInsertion} = apply(Ops, lists:nthtail(Quantity, Text), Cursor + Quantity, InsertionPoint),
      {lists:sublist(Text, Quantity) ++ ResultText, ResultInsertion};
    {retain, _Quantity} ->
      {ResultText, ResultInsertion} = apply(Ops, "", Cursor, InsertionPoint),
      {Text ++ ResultText, ResultInsertion};
    {insert, NewText} when Cursor < InsertionPoint ->
      NewInsertionPoint = InsertionPoint + length(NewText),
      {ResultText, ResultInsertion} = apply(Ops, Text, Cursor, NewInsertionPoint),
      {NewText ++ ResultText, ResultInsertion};
    {insert, NewText} ->
      {ResultText, ResultInsertion} = apply(Ops, Text, Cursor, InsertionPoint),
      {NewText ++ ResultText, ResultInsertion};
    {delete, OldText} when Cursor < InsertionPoint ->
      NewInsertionPoint = InsertionPoint - min(length(OldText), InsertionPoint - Cursor),
      apply(Ops, lists:nthtail(length(OldText), Text), Cursor + length(OldText), NewInsertionPoint);
    {delete, OldText} ->
      apply(Ops, lists:nthtail(length(OldText), Text), Cursor + length(OldText), InsertionPoint)
  end.

merge(X, []) -> X;
merge([], X) -> X;

%% MERGE RETAIN
merge([{retain, P} | Ops1], [{retain, P} | Ops2]) -> [{retain, P} | merge(Ops1, Ops2)];
merge([{retain, P1} | Ops1], [{retain, P2} | Ops2]) when P1 < P2 ->
  [{retain, P1} | merge(Ops1, [{retain, P2 - P1} | Ops2])];

%% MERGE INSERT ~ RETAIN
merge([{insert, T} | Ops1], [{retain, P} | Ops2]) -> [{insert, T} | merge(Ops1, [{retain, P} | Ops2])];

%% MERGE DELETE ~ RETAIN
merge([{delete, T} | Ops1], [{retain, P} | Ops2]) when length(T) == P -> [{delete, T} | merge(Ops1, Ops2)];
merge([{delete, T} | Ops1], [{retain, P} | Ops2]) when P > length(T) ->
  [{delete, T} | merge([{retain, P - length(T)} | Ops1], Ops2)];
merge([{delete, T} | Ops1], [{retain, P} | Ops2]) when P < length(T) ->
  [{delete, lists:sublist(T, 1, P)} | merge([{delete, lists:nthtail(P, T)} | Ops1], Ops2)];

%% MERGE INSERT
merge([{insert, T1} | Ops1], [{insert, T2} | Ops2]) when T1 =< T2 -> [{insert, T1}, {insert, T2} | merge(Ops1, Ops2)];

%% MERGE INSERT ~ DELETE
merge([{insert, T1} | Ops1], [{delete, T2} | Ops2]) -> [{insert, T1}, {delete, T2} | merge(Ops1, Ops2)];

%% MERGE DELETE
merge([{delete, T} | Ops1], [{delete, T} | Ops2]) -> [{delete, T} | merge(Ops1, Ops2)];
merge([{delete, T1} | Ops1], [{delete, T2} | Ops2]) when length(T1) > length(T2) ->
  [{delete, T2} | merge([{delete, lists:nthtail(length(T2), T1)} | Ops1], Ops2)];

merge(Ed1, Ed2) ->
  io:format("."),
  merge(Ed2, Ed1).