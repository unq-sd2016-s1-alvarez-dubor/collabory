%%%-------------------------------------------------------------------
%%% @author logain
%%% @copyright (C) 2016, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 09. jul 2016 01:50
%%%-------------------------------------------------------------------
-module(main).
-author("logain").


%% API
-export([refresh_frame/1, behaviour/2, start/0, stop/1]).

-include_lib("wx/include/wx.hrl").

-record(attr, {
  active_editors = [],
  frame,
  username,
  filename,
  share_key,
  logs
}).

-define(COLLABORY_MAIN, collabory_main).

start() ->
  spawn(fun() -> init() end).

init() ->
  case whereis(?COLLABORY_MAIN) of
    undefined -> register(?COLLABORY_MAIN, self()),
      This = make_window(),
      wxFrame:show(This#attr.frame),
      lifecycle:init_loop(This, module_info(module), [
        {before_receive, refresh_frame}
      ]);
    _ -> logger:e("Collabory is already running, please close it")
  end.

refresh_frame(This) ->
  wxFrame:refresh(This#attr.frame).

stop(This) ->
  lists:foreach(fun({_Name, Client, Monitor}) -> demonitor(Monitor), Client ! stop end, This#attr.active_editors),
  logger:d("Closing main window").

behaviour(This, Message) ->
  Key = utils:message_key(Message),
  case Key of
    new -> new(This);
    fork -> fork(This);
    {client_fail, FileName} -> log_to_window(This, "File ~s is already open", [FileName]);
    {fork_fail, ShareKey} -> log_to_window(This, "Conection to ~s failed", [ShareKey]);
    {new_client, ClientName, Client} -> new_client(This, ClientName, Client);
    {'DOWN', _, _, Client, _} -> remove_client(This, Client);
    _ -> logger:e("Unhandled message: ~w", [Message])
  end.

new(This) ->
  UserName = wxTextCtrl:getValue(This#attr.username),
  FileName = wxTextCtrl:getValue(This#attr.filename),
  peer:start(UserName, {file_name, FileName}).

fork(This) ->
  UserName = wxTextCtrl:getValue(This#attr.username),
  ShareKey = wxTextCtrl:getValue(This#attr.share_key),
  peer:start(UserName, {share_key, ShareKey}).

new_client(This, ClientName, Client) ->
  {state, This#attr{active_editors = [{ClientName, Client, monitor(process, Client)} | This#attr.active_editors]}}.

remove_client(This, Client) ->
  logger:d("Removing client ~w", [Client]),
  {state, This#attr{active_editors = lists:keydelete(Client, 1, This#attr.active_editors)}}.

log_to_window(This, Template) -> log_to_window(This, Template, []).
log_to_window(This, Template, Args) -> wxTextCtrl:appendText(This#attr.logs, io_lib:format(Template ++ "~n", Args)).

make_window() ->
  Self = self(),
  Width = 480,
  Height = 360,
  BorderPixels = 5,
  Server = wx:new(),
  Frame = wxFrame:new(Server, -1, "Welcome to Collabory", [{size, {Width, Height}}]),
  MainPanel = wxPanel:new(Frame),
  UserNameLabel = wxStaticText:new(MainPanel, 101, "Username"),
  UserNameTextBox = wxTextCtrl:new(MainPanel, 1002),
  FileNameLabel = wxStaticText:new(MainPanel, 102, "File name"),
  FileNameTextBox = wxTextCtrl:new(MainPanel, 1002),
  ShareKeyLabel = wxStaticText:new(MainPanel, 103, "Document share key"),
  ShareKeyTextBox = wxTextCtrl:new(MainPanel, 1003),
  ButtonOpen = wxButton:new(MainPanel, 10001, [{label, "Open"}]),
  wxEvtHandler:connect(ButtonOpen, command_button_clicked, [{callback, fun(_Obj, _Evt) -> Self ! fork end}]),
  ButtonNewFile = wxButton:new(MainPanel, 10002, [{label, "New document"}]),
  wxEvtHandler:connect(ButtonNewFile, command_button_clicked, [{callback, fun(_Obj, _Evt) -> Self ! new end}]),
  LogTextBox = wxTextCtrl:new(MainPanel, 1004, [{style, ?wxTE_READONLY bor ?wxTE_MULTILINE}]),
  wxEvtHandler:connect(Frame, close_window, [{callback, fun(_Obj, Evt) -> Self ! stop, wxEvent:skip(Evt) end}]),
  MainSizer = wxBoxSizer:new(?wxVERTICAL),
  UserNameSizer = wxBoxSizer:new(?wxHORIZONTAL),
  OptionsSizer = wxBoxSizer:new(?wxHORIZONTAL),
  NewFileSizer = wxBoxSizer:new(?wxVERTICAL),
  OpenFileSizer = wxBoxSizer:new(?wxVERTICAL),
  LogSizer = wxStaticBoxSizer:new(?wxVERTICAL, MainPanel, [{label, "Messages"}]),

  wxSizer:add(UserNameSizer, UserNameLabel, [{border, BorderPixels}, {flag, ?wxALL}]),
  wxSizer:add(UserNameSizer, UserNameTextBox, [{border, BorderPixels}, {flag, ?wxALL}]),

  wxSizer:add(NewFileSizer, FileNameLabel, [{border, BorderPixels}, {flag, ?wxBOTTOM bor ?wxCENTER}]),
  wxSizer:add(NewFileSizer, FileNameTextBox, [{border, BorderPixels}, {flag, ?wxBOTTOM bor ?wxCENTER}]),
  wxSizer:add(NewFileSizer, ButtonNewFile, [{proportion, 2}, {border, BorderPixels}, {flag, ?wxBOTTOM bor ?wxCENTER}]),

  wxSizer:add(OpenFileSizer, ShareKeyLabel, [{border, BorderPixels}, {flag, ?wxBOTTOM bor ?wxCENTER}]),
  wxSizer:add(OpenFileSizer, ShareKeyTextBox, [{border, BorderPixels}, {flag, ?wxBOTTOM bor ?wxCENTER}]),
  wxSizer:add(OpenFileSizer, ButtonOpen, [{proportion, 2}, {border, BorderPixels}, {flag, ?wxBOTTOM bor ?wxCENTER}]),

  wxSizer:add(LogSizer, LogTextBox, [{proportion, 2}, {border, BorderPixels}, {flag, ?wxALL bor ?wxEXPAND}]),

  wxSizer:add(OptionsSizer, NewFileSizer, [{proportion, 1}, {border, BorderPixels}, {flag, ?wxALL}]),
  wxSizer:add(OptionsSizer, OpenFileSizer, [{proportion, 1}, {border, BorderPixels}, {flag, ?wxALL}]),

  wxSizer:add(MainSizer, UserNameSizer, [{border, BorderPixels}, {flag, ?wxALL bor ?wxCENTER}]),
  wxSizer:add(MainSizer, OptionsSizer, [{border, BorderPixels}, {flag, ?wxALL bor ?wxCENTER}]),
  wxSizer:add(MainSizer, LogSizer, [{proportion, 1}, {border, BorderPixels}, {flag, ?wxALL bor ?wxCENTER bor ?wxEXPAND}]),

  wxPanel:setSizer(MainPanel, MainSizer),
  #attr{frame = Frame, username = UserNameTextBox, filename = FileNameTextBox, share_key = ShareKeyTextBox, logs = LogTextBox}.
