%%%-------------------------------------------------------------------
%%% @author Emanuel
%%% @copyright (C) 2016, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 14. jul 2016 13:49
%%%-------------------------------------------------------------------
-module(commit).
-author("Emanuel").

%% API
-export([get_tag/1, merge/2, get_ops/1, merge_pr/2]).


get_ops([]) -> [];
get_ops([{_Tag, Ops} | Commit]) -> [Ops | get_ops(Commit)].

get_tag([{Tag, _Ops} | _Commit]) -> Tag.

merge(Master, Commit) ->
  {CommitBegin, _Op} = lists:last(Commit),
  logger:d("Master ~w", [Master]),
  {Merge, Keep} = lists:splitwith(fun(Op = {Tag, _Ops}) -> logger:d("Op ~w", [Op]),
    not commit_tag:safe(CommitBegin, Tag) end, Master),
  Merged = merge_pr(lists:reverse(Merge), lists:reverse(Commit)),
  logger:d("Merge ~w", [Merge]),
  logger:d("Keep ~w", [Keep]),
  logger:d("Merged ~w", [Merged]),
  {lists:reverse(Merged) ++ Keep, Merge}.

merge_pr(X, Y) -> X ++ Y.

%%merge_pr([], B2) -> B2;
%%merge_pr(B1, []) -> B1;
%%merge_pr([{Tag1, X} | Xs], [{Tag2, Y} | Ys]) -> [{commit_tag:merge(Tag1, Tag2), ops:merge(X, Y)} | merge_pr(Xs, Ys)].