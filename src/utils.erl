%%%-------------------------------------------------------------------
%%% @author Emanuel
%%% @copyright (C) 2016, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 14. jul 2016 1:30
%%%-------------------------------------------------------------------
-module(utils).
-author("Emanuel").

%% API
-export([broadcast/2, generate_register_name/1, encode_share_key/1, decode_share_key/1, message_key/1]).

broadcast(Targets, Message) ->
  lists:foreach(fun(Target) -> logger:d("Target ~w", [Target]), Target ! Message end, Targets).

generate_register_name(FileName) -> list_to_atom(FileName).

encode_share_key(RegisterName) -> encoding:tuple_to_token({RegisterName, node()}).

decode_share_key(ShareKey) -> encoding:token_to_tuple(ShareKey).

message_key({wx, _, _, _, Event}) -> element(2, Event);
message_key(Message) -> Message.