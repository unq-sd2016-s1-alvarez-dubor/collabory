%%%-------------------------------------------------------------------
%%% @author logain
%%% @copyright (C) 2016, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 10. jul 2016 23:34
%%%-------------------------------------------------------------------
-module(encoding).
-author("logain").

%% API
-export([tuple_to_token/1, token_to_tuple/1]).

hex(N) when N < 10 ->
  $0+N;
hex(N) when N >= 10, N < 16 ->
  $a+(N-10).

int(C) when $0 =< C, C =< $9 ->
  C - $0;
int(C) when $A =< C, C =< $F ->
  C - $A + 10;
int(C) when $a =< C, C =< $f ->
  C - $a + 10.

to_hex(N) when N < 256 ->
  [hex(N div 16), hex(N rem 16)].

list_to_hexstr([]) ->
  [];
list_to_hexstr([H|T]) ->
  to_hex(H) ++ list_to_hexstr(T).

tuple_to_token(Touple) ->
  list_to_hexstr(binary_to_list(term_to_binary(Touple))).

token_to_tuple(S) ->
  binary_to_term(list_to_binary(hexstr_to_list(S))).

hexstr_to_list([X,Y|T]) ->
  [int(X)*16 + int(Y) | hexstr_to_list(T)];
hexstr_to_list([]) ->
  [].