%%%-------------------------------------------------------------------
%%% @author logain
%%% @copyright (C) 2016, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 10. jul 2016 20:07
%%%-------------------------------------------------------------------
-module(merge_tests).
-include_lib("eunit/include/eunit.hrl").
-author("logain").

%% API
-export([]).

apply_insert_to_text_test() ->
  {"ABC12DE", 0} = ops:apply([{retain, 3}, {insert, "12"}, {retain, 2}], "ABCDE", 0).

apply_delete_to_text_test() ->
  {"ABDE", 0} = ops:apply([{retain, 2}, {delete, "C"}, {retain, 2}], "ABCDE", 0).

apply_delete_with_insert_to_text_test() ->
  {"AB12DE", 0} = ops:apply([{retain, 2}, {delete, "C"}, {insert, "12"}, {retain, 2}], "ABCDE", 0).

apply_insert_with_delete_to_text_test() ->
  {"AB12DE", 0} = ops:apply([{retain, 2}, {insert, "12"}, {delete, "C"}, {retain, 2}], "ABCDE", 0).

merge_inserts_converge_test() ->
  Ops1 = [{insert, "12"}],
  Ops2 = [{insert, "34"}],
  "1234" = merge(Ops1, Ops2),
  "1234" = merge(Ops2, Ops1).

merge_inserts_with_retain_converge_test() ->
  Text = "ABCDEFG",
  Ops1 = [{retain, 2}, {insert, "1234"}], %% "ABCDEFG" => "ABC1234DEFG"
  Ops2 = [{insert, "34"}],
  "34AB1234CDEFG" = merge(Ops1, Ops2, Text),
  "34AB1234CDEFG" = merge(Ops2, Ops1, Text).

merge_when_insert_overlaps_delete_converge_test() ->
  Text = "ABCDEFG",
  Ops1 = [{retain, 2}, {insert, "1234"}],
  Ops2 = [{delete, "ABC"}],
  "1234DEFG" = merge(Ops1, Ops2, Text),
  "1234DEFG" = merge(Ops2, Ops1, Text).

merge_when_delete_overlaps_insert_converge_test() ->
  Text = "ABCDEFG",
  Ops1 = [{insert, "1234"}],
  Ops2 = [{retain, 2}, {delete, "CDE"}],
  "1234ABFG" = merge(Ops1, Ops2, Text),
  "1234ABFG" = merge(Ops2, Ops1, Text).

merge_when_delete_overlaps_delete_converge_test() ->
  Text = "ABCDEFG",
  Ops1 = [{retain, 3}, {delete, "DE"}],
  Ops2 = [{retain, 1}, {delete, "BCD"}],
  "AFG" = merge(Ops1, Ops2, Text),
  "AFG" = merge(Ops2, Ops1, Text).

merge_multiple_inserts_converge_test() ->
  [{retain,1},{insert,"x"},{retain,1},{insert,"x"},{retain,1}] =
    ops:merge(
      [{retain,2},{insert,"x"},{retain,1}],
      [{retain,1},{insert,"x"}]
    ).


merge_large_retain_converge_test() ->
  Text = "ABCD",
  Ops1 = [{insert,"1"},{insert,"2"},{insert,"3"},{insert,"4"}], %% 1234ABCD
  Ops2 = [{retain,4},{insert,"x"}],                             %% ABCDx
  "1234ABCDx" = merge(Ops1, Ops2, Text),
  "1234ABCDx" = merge(Ops2, Ops1, Text).

merge(Ops1, Ops2) -> merge(Ops1, Ops2, "").
merge(Ops1, Ops2, Text) -> {T, _} = ops:apply(ops:merge(Ops1, Ops2), Text, 0), T.