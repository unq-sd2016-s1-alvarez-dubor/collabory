%%%-------------------------------------------------------------------
%%% @author root
%%% @copyright (C) 2016, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 23. jun 2016 20:27
%%%-------------------------------------------------------------------
-module(logger).
-author("root").

%% API
-export([e/2, w/2, d/2, e/1, w/1, d/1]).

-define(Error_Level, 3).
-define(Warning_Level, 2).
-define(Debug_Level, 1).
-define(Log_Level, ?Debug_Level).
-define(Level_Strings, {"Debug", "Warning", "Error"}).

key_to_level(Level) -> element(Level, ?Level_Strings).

log(Level, String, Args) when ?Log_Level =< Level ->
  io:format("[~s] ~s~n", [key_to_level(Level), io_lib:format(String, Args)]);
log(_, _, _) -> ok.

e(String) -> e(String, []).
e(String, Args) -> log(?Error_Level, String, Args).
w(String) -> w(String, []).
w(String, Args) -> log(?Warning_Level, String, Args).
d(String) -> d(String, []).
d(String, Args) -> log(?Debug_Level, String, Args).

