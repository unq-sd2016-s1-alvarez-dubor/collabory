%%%-------------------------------------------------------------------
%%% @author Emanuel
%%% @copyright (C) 2016, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 20. jun 2016 20:11
%%%-------------------------------------------------------------------
-module(run).
-author("root").

%% API
-export([now/0, app/0]).

now() ->
  r3:do(compile),
  main:start().

app() ->
  main:start().