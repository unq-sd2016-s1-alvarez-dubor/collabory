Collabory
=====

Collabory es un editor de textos colaborativo, que permite a diferentes usuarios trabajar de manera remota sobre el mismo documento.
Al abrir el programa el usuario se encuentra con la opción de crear un nuevo documento o abrir un documento compartido, ingresando un _share token_
Al seleccionar la opción de crear nuevo documento, se presenta la pantalla de edición y un token que puede usar para compartirlo con otras personas.

## Arquitectura

Se utilizó una arquitectura descentralizada, donde existen dos tipos de nodos: _Peer_ y _ClientGUI_, y hay un _ClientGUI_ por cada _Peer_

Para sincronizar la edición en tiempo real se aplicó la técnica de _Operational Transformation_ que convierte la edición en operaciones de transformaciones sobre el texto.
A su vez, cada operación es identificada con un _Vector Clock_ para facilitar el mergeo de operaciones entre clientes y resolución de conflictos.

### Operational Transformation

Al ingresar texto en la interfaz gráfica, éste es convertido a una lista de operaciones primitivas:

 - `insert` inserta texto a partir de la posición del cursor
 - `delete` borra texto a partir de la posición del cursor
 - `retain` incrementa la posición del cursor

Estas operaciones son enviadas a su _Peer_, el cual se encargará de hacerlas conocer al resto de los Peers que trabajen sobre el mismo documento.
Para ello la aplicación de operaciones locales y remotas deben cumplir:

 - Precedencia: se asegura la aplicación ordenada de las operaciones, según fueron emitidas. La relación entre dos operaciones queda dada por vector clocks.
 - Convergencia: las diferentes copias del documento se mantienen sincronizadas. Al aplicar todas las operaciones, todos los usuarios ven el mismo documento resultante.


Toda edición se compone de un conjunto de operaciones totales sobre el texto, es decir que consumen exactamente todos los caracteres del texto.
Por ejemplo, al editar el texto:

```
ABC
```

y convertirlo en:

```
A12C
```

Se realizaron las operaciones:

```
retain 1
delete B
insert 12
retain 1
```

Al recibir una edición nueva, el cliente la mergea con sus cambios no publicados.
Para cada operación de una edición, se compara con la operación de la otra edición, aumentando sus cursores, de manera tal que:

- Si op1 agrega caracteres antes que op2, se incrementa el cursor de op2.

- Si op1 opera después que op2, no se hace nada (se continpua analizando la siguiente operación).

- Si op1 agrega texto en un rango en el que op2 realiza un borrado, se separa la operación de obrrado en dos rangos y se coloca la operación de insert en el medio.

- Si tanto op1 como op2 borran, se fusionan en una única operación.

Estas transformaciones se pueden aplicar sólo entre operaciones que parten del mismo estado.


### Share Token

Para poder editar un documento existente, un usuario debe compartir con otro el token correspondiente a su documento.
Este token es un string alfanumérico que contiene la información de IP, nombre del nodo y nombre del archivo.

Al abrir un documento, se extrae del token la información necesaria para conectarse con un nodo de la red y pedirle el estado actual del documento y los _Peers_ que se encuentran actualmente editándolo. A su vez, todos los otros _Peers_ pasan a conocer al nuevo _Peer_.





